#include <stdio.h>
#include "test_frame.h"
#include "../src/hello.h"

int test_hello(char* expected, char* actual)
{
	printf("%s, %s\n", expected, actual);
	return expect_str(expected, actual);
}

int main(void)
{
	int passed = 0;
	int total = 0;

	// tset area

	passed += test_hello(hello(), "Hello!\n");
	total++;

	// end test area

	printf("Passed %d of %d tests.\n", passed, total);
	return passed == total ? 0 : 1;
}


