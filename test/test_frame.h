#ifndef test_frame_h
#define test_frame_h

int expect_str(char* s1, char* s2);
int expect_int(int num1, int num2);
int expect_within(float num1, float num2, float epsilon);

#endif
