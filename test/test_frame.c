#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int expect_str(char* s1, char* s2)
{
	int result = strcmp(s1, s2);
	if(result == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int expect_int(int num1, int num2)
{
	if(num1 == num2)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int expect_within(float num1, float num2, float epsilon)
{
	float result = abs(num1 - num2);
	if(result < epsilon)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
