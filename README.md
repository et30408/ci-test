# Continuous Integration Test

## To execute

Requirements:

- `clang` version 3.5 or higher
- `cmake` version 3.0 or higher

### Building, running, and testing

Make the build:

	cd build
	cmake ..
	make

To run, do `./main`. To test, do `make test`.


## Structure

	/src
		Libraries
	/test
		Unit tests for libraries
	/build
		The location for the CMake build

## To do

- Set up CI with Jenkins
- Fail some tests, pass some tests
	- let's get some real spicy building action
 
