# minimum required version of cmake
cmake_minimum_required(VERSION 3.0)

# project name
project(ciTest)

# include the following directories (find header files)
include_directories("${PROJECT_SOURCE_DIR}/src")
include_directories("${PROJECT_SOURCE_DIR}/test")

# include the following subdirectories (search for libs and execs)
add_subdirectory(src)
add_subdirectory(test)

# add main executable
add_executable(main main.c)

# add libraries for main
target_link_libraries(main hello)

# enable testing
enable_testing()

# tests
add_test(NAME testHello COMMAND testHello)
